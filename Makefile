ldflags := "-s -w"
gcflags := ""
flags := -ldflags=${ldflags} -gcflags=${gcflags}

.PHONY: build-service
build-service: prebuild
	go build ${flags} -o bin cmd/service/service.go

.PHONY: build-docker
build-docker: docker-lint clean fmt
	docker buildx build \
		-t meli-tech-test .; \
	make clean

.PHONY: docker-compose-up
docker-compose-up: prebuild docker-lint secrets tls-certs
	docker-compose up --build -d

.PHONY: docker-compose-down
docker-compose-down:
	docker-compose down

.PHONY: docker-compose-cleanall
docker-compose-cleanall:
	docker-compose down --volumes

.PHONY: secrets
secrets: meli_app_id.txt meli_secret_key.txt

meli_app_id.txt:
	@read -p "What is the App Id: " appid; \
	echo $${appid} > meli_app_id.txt

meli_secret_key.txt:
	@read -p "What is the Secret Key: " secretkey; \
	echo $${secretkey} > meli_secret_key.txt

.PHONY: tls-certs
tls-certs: server.key server.crt

.PHONY: prebuild
prebuild: vendor fmt

.PHONY: test
test:
	go test -v ./...

server.key:
	openssl genrsa -out server.key 2048

server.crt: server.key
	openssl req -new -x509 -sha256 -key server.key -out server.crt -days 3650

.PHONY: docker-lint
docker-lint:
	docker run --rm -i ghcr.io/hadolint/hadolint < Dockerfile

bin:
	mkdir -p bin

.PHONY: fmt
fmt: staticcheck
	go fmt ./...

.PHONY: staticcheck
staticcheck: vet
	go install honnef.co/go/tools/cmd/staticcheck@latest
	$(GOPATH)/bin/staticcheck ./...

.PHONY: vet
vet:
	go vet ./...

.PHONY: vendor
vendor: tidy
	go mod vendor

.PHONY: tidy
tidy:
	go mod tidy

.PHONY: clean
clean:
	rm -rf bin/*
