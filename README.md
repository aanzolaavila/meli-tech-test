# Prueba tecnica de Mercado Libre

## Desafio practico

### Requerimientos
Este proyecto necesita de:
 - Docker
 - Docker compose
 - GNU Make (para construir el proyecto mas facilmente)
 - OpenSSL (opcional: para crear certificados dummy)
 - Una aplicacion creada en Mercado Libre (AppId y Secret Key)

**Recomendado**: Ejecutar el proyecto en un entorno Unix: macOS o Linux.

### Correr
Para correr este proyecto, si posee de `GNU make` tan solo consta de hacer
```bash
$ make docker-compose-up
```

Si es la primera vez que se corre el proyecto, pedira que se creen los secretos (el AppId y la llave secreta de la aplicacion de Mercado Libre), `make` preguntara por cada una en la linea de comandos y creara los archivos `meli_app_id.txt` y `meli_secret_key.txt` respectivamente para mayor facilidad.

Ademas de esto se pedira que se realice la generacion de una llave publica y privada con `OpenSSL` desde el comando de `make`, esto se describe mas adelante por que es necesario.

#### 1. OAuth Login
Para el proceso de autenticacion con MELI, dado que utiliza OAuth, debemos abrir una URL generada por el programa. Para esto debemos mirar la salida estandar del programa en el contenedor donde se ejecuta.

Para verificar el identificador del contenedor podemos hacer:
```bash
$ docker ps
```

Ahi se podra visualizar los contenedores que estan corriendo, ubicamos el contenedor donde esta corriendo el programa (en la imagen es el primero).

Y luego se ejecuta:
```bash
$ docker logs -f <container-id>
```

Ahi podremos visualizar que la aplicacion esta requiriendo de que se abra un link en el navegador web.

![Obtener logs de contenedor](img1.png)

Nos dirigimos a la URL, autorizamos al programa de hacer uso de la aplicacion de Mercado Libre. Luego de esto somos redirigidos a `https://127.0.0.1/oauth/redirect`, donde esta corriendo el programa para capturar el Refresh Token. Dado que el certificado del sitio no lo conoce el navegador ni esta en ninguna entidad verificadora, se debe indicar al navegador de que confie en el certificado. Luego de esto el servidor contestara con `SUCCESS: Got the code` y el servidor TLS se cerrara.

Inmediatamente despues de esto, el programa iniciara el servicio Web donde se estaran escuchando las peticiones. Asumiendo que se pudo conectar correctamente a la base de datos.


##### Creacion de certificados y llave privada SSL
MELI requiere que las URLs de redireccion para la autenticacion con OAuth deben utilizar protocolo `HTTPS`, por lo que como alternativa tomada en este proyecto fue de utilizar un servidor temporal con `TLS` para esperar la respuesta del servicio de autenticacion y extraer el codigo de autenticacion para obtener el Refresh Token.

Ya que para utilizar TLS es necesario tener tanto un certificado como una llave privada, se hace la creacion de estos de manera automatica en el comando `make`. Ya que estos no estan siendo verificados con ninguna entidad verificadora (e.j. Verisign), el navegador web no confiara en el certificado, por lo que se debe indicarle al navegador a confie en el certificado.


#### 2. Rutas de la API

La aplicacion correra el API en el puerto `8080`. Si se desea un puerto distinto, se puede indicar el puerto con la variable de entorno `$PORT` (Heredada de `gin`). *En el caso de docker-compose, indicarla en el archivo*.

Las rutas disponibles publicas del API REST son:

##### `/healthcheck`
Solo responde `200`: `Service is healthy`

##### `/v1/queries/file/csv`
Espera un archivo CSV como entrada, para probar este endpoint puede hacer
```bash
$ curl -X POST localhost:8080/v1/queries/file/csv \
  -F "file=@technical_challenge_data.csv" \
  -H "Content-Type: multipart/form-data"
```

El servicio respondera con `file is being processed`. Al mismo tiempo se esta procesando en el programa y siendo agregadas las consultas a la base de datos.

#### 3. Cerrar aplicacion

Para parar la aplicacion y la base de datos puede hacerse con:
```bash
$ make docker-compose-down
```

Esto no borrara el volumen donde esta guardada la informacion guardada por la base de datos. Para borrarla, puede hacerse:
```bash
$ make docker-compose-cleanall
```

---

## Desafio teorico

### Procesos, hilos y corrutinas

> Un caso en el que usarías procesos para resolver un problema y por qué.

Un caso de uso puede ser cuando se requiere tener mas tolerancia a fallos, un caso de ejemplo es Google Chrome: este utiliza un proceso principal que crea y administra otros procesos: las pestañas.

En terminos de memoria, cada proceso tiene:
- Su propio espacio de memoria direccionable
- Su propio Heap
- Su propio Stack

Esto implica que el fallo irrecuperable de un proceso no implica el fallo de los otros.

Sin embargo, entre los aspectos negativos se tiene que cada proceso es costoso de construir y si se requiere comunicacion entre ellos (con Inter Process Communication, administrado por el SO), este tiende a ser lento. Adicionalmente, ya que estos no comparten memoria, solo se pueden usar los medios de comunicacion del SO para coordinar. El SO aplica diferentes prioridades para cada proceso, por lo que no se puede asegurar que proceso tendra mas tiempo de CPU.

En caso de un fallo para una pestaña en Google Chrome, lo que el navegador muestra es que la pestaña fallo.

> Un caso en el que usarías threads para resolver un problema y por qué.

Un caso de uso es cuando se quieren realizar multiples actividades de manera concurrente que requiere un nivel de coordinacion mayor que un proceso, y donde cada actividad requiere hacer un proceso largo antes de acabar.

En terminos de memoria, cada hilo tiene:
 - Espacio de memoria direccionable compartida
 - Heap compartido
 - Su propio Stack

Al tener un modelo de memoria compartida, es mas facil coordinar tareas que hacen la misma actividad, de esta manera se pueden asignar mas recursos del sistema (si se puede utilizar multiples CPU), y de esta manera solo coordinar de que terminen y que el recurso compartido este en un estado consistente con acceso mutuamente exclusivo, o con una estructura de datos tolerante a un ambiente concurrente.

Un caso de ejemplo puede ser los compiladores de C/C++, cuando se esta realizando la compilacion de multiples archivos, cada archivo puede considerarse como una unidad de compilacion, de manera que es posible coordinar la compilacion de cada uno de manera concurrente, y al final del proceso conectar cada unas de las unidades en un archivo ejecutable.

Un aspecto negativo es que la candidad de hilos que se pueden instanciar en el sistema esta bastante limitado a lo dictaminado al sistema operativo, por lo que se puede llegar a un limite si se instancian muchos hilos en una actividad, y este valor es muy relativo a los recursos del SO y las limitaciones que este imponga. Adicionalmente, si el proceso dueño de los hilos falla, todos los hilos dejan de existir junto con el proceso.

Finalmente, en cuanto a la prioridad de una actividad, esta es dictaminada por el administrador de tareas del SO (`scheduler`).

> Un caso en el que usarías corrutinas para resolver un problema y por qué.

El caso mas popular es el manejo de multiples eventos de manera concurrente en actividades de corta duracion y que pueden cambiar dinamicamente segun el uso: servidores web.

En terminos de memoria, cada corrutina tiene los mismo aspectos que los hilos.

En terminos operacionales, las corrutinas son coordinadas por un administrador externo (`scheduler` interno del programa, heredado usualmente del lenguaje que se uso).

Las corrutinas tambien son conocidas como 'hilos verdes', esto debido a que el administrador de tareas interno instancia varios hilos, donde cada hilo es asignado a ejecutar una corrutina en un tiempo determinado, si el administrador determina que la carga deberia irse a otras actividades para ser mas equitativo en el uso de los recursos, entonces este puede hacer un cambio de corrutina en el hilo. Esto tambien implica que si una corrutina esta esperando un evento, el administrador puede asignar el hilo dinamicamente.

En el caso de los servidores web, asumiendo que se tengan multiples rutas por donde se pueden recibir peticiones, el trafico en cada uno de estas puede variar dinamicamente en el tiempo, haciendo que la asignacion de cada corrutina a un hilo sea importante que se haga de manera eficiente.

### Optimizacion de recursos del sistema operativo

> Si tuvieras 1.000.000 de elementos y tuvieras que consultar para cada uno de ellos información en una API HTTP. ¿Cómo lo harías? Explicar.

Si el API permite incluir varias consultas en un solo llamado, se pueden hacer pequeños conjuntos de elementos (batches) donde se pueden consolidar en una sola peticion, y de esta manera realizar varios llamados concurrentes con con cada batch. El tamaño de cada batch dependera de la velocidad de respuesta del API.

Luego para el procesamiento de cada peticion, se incluirian el manejo de errores, en caso de que uno de los elementos cause un error generalizado de la peticion, se podria hacer un cambio correctivo para hacer el reintento.

Para este proceso usaria corrutinas, y de ser posible patrones de comunicacion con colas entre las diferentes corrutinas (como Go), de esta manera, a medida que se vaya recibiendo las respuestas del servidor, se pueden ir procesando las respuestas a medida que van llegando.

### Analisis de complejidad

> Dados 4 algoritmos **A, B, C y D** que cumplen la misma funcionalidad, con complejidades **O(n^2)**, **O(n^3)**, **O(2^n)** y **O(n log n)**, respectivamente, ¿Cuál de los algoritmos favorecerías y cuál descartarías en principio? Explicar por qué.

En escala logaritmica, el orden de prioridad para cada algoritmo es dado de esta manera:

En orden:
 - D
 - A
 - B
 - C

Esto debido a que a medida que se tienen a valores mas grandes de **n**, el comportamiento de cada algoritmo se vuelve apartente.

![Comparacion algoritmos](./img2.png)

> Asume que dispones de dos bases de datos para utilizar en diferentes problemas a resolver. La primera llamada *AlfaDB* tiene una complejidad de **O(1)** en consulta y **O(n^2)** en escritura. La segunda llamada **BetaDB** que tiene una complejidad de **O(log n)** tanto para consulta, como para escritura. ¿Describe en forma sucinta, qué casos de uso podrías atacar con cada una?

**AlfaDB** la usaria para casos en que:
 - Se guarden muy pocos datos
 - Es crucial la velocidad de consulta: va a tener muy alto consumo

**AlfaDB** la usaria para casos en que:
 - El volumen de datos es considerablemente alto
 - La frecuencia de lectura como de escritura es aproximadamente el mismo
