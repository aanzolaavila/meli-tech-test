FROM --platform=$BUILDPLATFORM golang:1.19.0-alpine as builder

ARG TARGETOS
ARG TARGETARCH

WORKDIR /src

COPY go.mod go.sum ./
RUN go mod download && go mod verify

ENV GOOS=${TARGETOS} GOARCH=${TARGETARCH} CGO_ENABLED=0

COPY . .
RUN GOOS=${GOOS} GOARCH=${GOARCH} CGO_ENABLED=${CGO_ENABLED} \
		 go build \
		 -ldflags="-s -w" \
		 -o /usr/local/bin/main cmd/service/service.go

# ---

FROM builder as test

RUN go test -v ./... && \
		touch /empty

# ---

FROM --platform=$BUILDPLATFORM alpine:3.16.0

WORKDIR /

COPY --from=test /empty .

COPY server.crt server.key ./
COPY --from=builder /usr/local/bin/main ./main

ENTRYPOINT [ "/main"]
