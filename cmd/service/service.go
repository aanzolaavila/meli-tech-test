package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"unicode"

	"github.com/aanzolaavila/meli-technical-test/internal/api/rest"
	"github.com/aanzolaavila/meli-technical-test/internal/auth/meli"
	"github.com/aanzolaavila/meli-technical-test/internal/helpers"
	"github.com/aanzolaavila/meli-technical-test/internal/http/queries"
	authrepository "github.com/aanzolaavila/meli-technical-test/internal/repository/auth/relational"
	entityrepo "github.com/aanzolaavila/meli-technical-test/internal/repository/entities/relational"
	meliservice "github.com/aanzolaavila/meli-technical-test/internal/services/meli"
	"golang.org/x/net/context"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const (
	AppIdEnvName      = "MELI_APP_ID_FILE"
	SecretKeyEnvName  = "MELI_SECRET_KEY_FILE"
	DbUserEnvName     = "DB_USER"
	DbPassEnvName     = "DB_PASS"
	DbHostEnvName     = "DB_HOST"
	DbDatabaseEnvName = "DB_DATABASE"
)

type Config struct {
	AppIdFile     string
	SecretKeyFile string
	DbUser        string
	DbPass        string
	DbHost        string
	DbDatabase    string
}

func validateConfig(config Config) error {
	if config.AppIdFile == "" {
		return fmt.Errorf("app id cannot be empty")
	}

	if config.SecretKeyFile == "" {
		return fmt.Errorf("secret key cannot be empty")
	}

	return nil
}

func GetConfig() (Config, error) {
	var config Config

	flag.StringVar(&config.AppIdFile, "appid", os.Getenv(AppIdEnvName), "App ID Secret location")
	flag.StringVar(&config.SecretKeyFile, "secretkey", os.Getenv(SecretKeyEnvName), "Secret Key Secret location")
	flag.StringVar(&config.DbUser, "dbuser", os.Getenv(DbUserEnvName), "DB User")
	flag.StringVar(&config.DbPass, "dbpass", os.Getenv(DbPassEnvName), "DB Pass")
	flag.StringVar(&config.DbHost, "dbhost", os.Getenv(DbHostEnvName), "DB Host")
	flag.StringVar(&config.DbDatabase, "dbdatabase", os.Getenv(DbDatabaseEnvName), "DB Database")

	flag.Parse()

	if err := validateConfig(config); err != nil {
		return Config{}, err
	}

	return config, nil
}

func readFileAsString(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}

	content, err := io.ReadAll(file)
	if err != nil {
		return "", err
	}

	// Hidden characters can be in the content
	cleanedContent := strings.TrimFunc(string(content), func(r rune) bool {
		return !unicode.IsGraphic(r)
	})

	return cleanedContent, nil
}

func getCredentialsFromFiles(appIdFile, appSecretKeyFile string) (appId, secretKey string, err error) {
	appId, err = readFileAsString(appIdFile)
	if err != nil {
		return
	}

	secretKey, err = readFileAsString(appSecretKeyFile)
	return
}

func getDB(user, pass, host, database string) (*gorm.DB, error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", user, pass, host, database)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	return db, err
}

func main() {
	ctx := context.Background()

	config, err := GetConfig()
	if err != nil {
		log.Fatalf("error getting config: %v\n", err)
	}

	appId, secretKey, err := getCredentialsFromFiles(config.AppIdFile, config.SecretKeyFile)
	if err != nil {
		log.Fatalf("could not get secrets: %v\n", err)
	}

	db, err := getDB(config.DbUser, config.DbPass, config.DbHost, config.DbDatabase)
	if err != nil {
		log.Fatalf("error connecting to DB: %v\n", err)
	}

	authenticator := meli.GetDefaultAuthenticator()
	authrepo, err := authrepository.NewRepository(db)
	if err != nil {
		log.Fatalf("could not create auth repository: %v", err)
	}

	authConfig := authenticator.GetBaseOAuthConf(appId, secretKey)
	token, err := helpers.GetToken(ctx, authrepo, authenticator, authConfig)
	if err != nil {
		log.Fatalf("error getting oauth token: %v\n", err)
	}

	client := authenticator.GetOAuthClientFromToken(ctx, authConfig, token)

	meliClient, err := queries.DefaultMeliClient(client)
	if err != nil {
		log.Fatalf("error creating meli client: %v\n", err)
	}

	repo, err := entityrepo.NewRepository(db)
	if err != nil {
		log.Fatalf("could not create entities repository: %v", err)
	}

	service, err := meliservice.NewService(repo, meliClient)
	if err != nil {
		log.Fatalf("error creating meli client: %v\n", err)
	}

	restService, err := rest.NewRestServiceApi(service)
	if err != nil {
		log.Fatalf("error creating meli client: %v\n", err)
	}

	fmt.Println("Starting service ...")
	err = restService.StartService()
	if err != nil {
		log.Fatalf("error starting service: %v\n", err)
	}
}
