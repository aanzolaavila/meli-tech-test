package entities

import "time"

type SiteID string
type ItemID string
type CurrencyID string
type CategoryID string
type UserID int64

type Base struct {
	SiteId SiteID
	ItemId ItemID
}

type Item struct {
	Base
	Price      float64
	StartTime  time.Time
	CategoryId CategoryID
	CurrencyId CurrencyID
	SellerId   UserID
}

type Category struct {
	Id   CategoryID
	Name string
}

type Currency struct {
	Id          CurrencyID
	Description string
}

type User struct {
	Id       UserID
	Nickname string
}

type Entry struct {
	Item     Item
	Category Category
	Currency Currency
	User     User
}
