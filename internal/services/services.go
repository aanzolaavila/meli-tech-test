package services

type QueryService[T any] interface {
	ProcessAsync(<-chan T) error
}
