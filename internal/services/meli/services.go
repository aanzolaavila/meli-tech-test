package impl

import (
	"fmt"
	"log"
	"os"

	"github.com/aanzolaavila/meli-technical-test/internal/entities"
	"github.com/aanzolaavila/meli-technical-test/internal/http/queries"
	"github.com/aanzolaavila/meli-technical-test/internal/pipeline"
	repository "github.com/aanzolaavila/meli-technical-test/internal/repository/entities"
)

type MeliQueryService struct {
	Repo       repository.Repository
	MeliClient queries.MeliApiClient
}

func NewService(repo repository.Repository, client queries.MeliApiClient) (*MeliQueryService, error) {
	if repo == nil {
		return nil, fmt.Errorf("repo cannot be nil")
	}

	if client == nil {
		return nil, fmt.Errorf("meli client cannot be nil")
	}

	return &MeliQueryService{
		Repo:       repo,
		MeliClient: client,
	}, nil
}

func handleRequestFunc(client queries.MeliApiClient) pipeline.Handler[entities.Base, entities.Entry] {
	emptyEntry := entities.Entry{}

	r := func(base entities.Base) pipeline.Result[entities.Entry] {
		res := entities.Entry{}

		site := string(base.SiteId)
		item := string(base.ItemId)
		itemId := site + item

		it, err := client.GetItem(entities.ItemID(itemId))
		if err != nil {
			return pipeline.Result[entities.Entry]{Error: err, Value: emptyEntry}
		}
		res.Item = it

		cat, err := client.GetCategory(it.CategoryId)
		if err != nil {
			return pipeline.Result[entities.Entry]{Error: err, Value: emptyEntry}
		}
		res.Category = cat

		cur, err := client.GetCurrency(it.CurrencyId)
		if err != nil {
			return pipeline.Result[entities.Entry]{Error: err, Value: emptyEntry}
		}
		res.Currency = cur

		user, err := client.GetUser(it.SellerId)
		if err != nil {
			return pipeline.Result[entities.Entry]{Error: err, Value: emptyEntry}
		}
		res.User = user

		return pipeline.Result[entities.Entry]{
			Error: nil,
			Value: res,
		}
	}

	return r
}

func handleRepositoryFunc(repo repository.Repository) pipeline.Handler[entities.Entry, entities.ItemID] {
	const nilId entities.ItemID = ""

	r := func(entry entities.Entry) pipeline.Result[entities.ItemID] {
		log.Printf("Writing into db entry with id %s\n", entry.Item.ItemId)

		err := repo.SaveEntry(entry)
		if err != nil {
			return pipeline.Result[entities.ItemID]{Value: nilId, Error: err}
		}

		return pipeline.Result[entities.ItemID]{Value: entry.Item.ItemId, Error: nil}
	}

	return r
}

func (s MeliQueryService) startPipeline(done <-chan struct{}, stream <-chan entities.Base) <-chan entities.ItemID {
	entriesStream := pipeline.ConcurrentMap(done, 20,
		stream, handleRequestFunc(s.MeliClient))

	filteredEntries := pipeline.OnError(done, entriesStream,
		func(entry entities.Entry, err error) {
			fmt.Fprintf(os.Stderr, "Error processing entry with id [%v]: %v\n", entry.Item.ItemId, err)
		})

	savedEntries := pipeline.Map(done, filteredEntries, handleRepositoryFunc(s.Repo))

	sucessfullSavings := pipeline.OnError(done, savedEntries,
		func(id entities.ItemID, err error) {
			fmt.Printf("Failed to save entry with item id %s: %+v\n", id, err)
		})

	return sucessfullSavings
}

func (s MeliQueryService) ProcessAsync(input <-chan entities.Base) error {
	if input == nil {
		return fmt.Errorf("input channel cannot be nil")
	}

	done := make(chan struct{})

	processedEntries := s.startPipeline(done, input)

	go func(done <-chan struct{}, entries <-chan entities.ItemID) {
		pipeline.WaitClosed(done, entries)
	}(done, processedEntries)

	return nil
}
