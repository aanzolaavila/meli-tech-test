package queries

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/aanzolaavila/meli-technical-test/internal/entities"
	queryTypes "github.com/aanzolaavila/meli-technical-test/internal/http/queries/types"
)

const (
	EndpointAPI = "https://api.mercadolibre.com"
)

type MeliApiClient interface {
	GetItem(entities.ItemID) (entities.Item, error)
	GetCategory(entities.CategoryID) (entities.Category, error)
	GetCurrency(entities.CurrencyID) (entities.Currency, error)
	GetUser(entities.UserID) (entities.User, error)
}

type meliImpl struct {
	Client *http.Client
}

func DefaultMeliClient(client *http.Client) (MeliApiClient, error) {
	if client == nil {
		return nil, fmt.Errorf("client cannot be nil")
	}

	return &meliImpl{
		Client: client,
	}, nil
}

func doGet(client *http.Client, url *url.URL, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(http.MethodGet, url.String(), body)
	if err != nil {
		return nil, err
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func getBaseUrl() *url.URL {
	url, err := url.Parse(EndpointAPI)
	if err != nil {
		panic(err)
	}
	return url
}

func (c *meliImpl) GetItem(itemId entities.ItemID) (entities.Item, error) {
	emptyItem := entities.Item{}

	if itemId == "" {
		return emptyItem, fmt.Errorf("item id cannot be nil")
	}

	url := getBaseUrl().JoinPath("items")
	q := url.Query()
	q.Add("ids", string(itemId))
	url.RawQuery = q.Encode()

	res, err := doGet(c.Client, url, nil)
	if err != nil {
		return emptyItem, err
	}

	out, err := io.ReadAll(res.Body)
	if err != nil {
		return emptyItem, err
	}

	var response queryTypes.ItemResponse
	if err := json.Unmarshal(out, &response); err != nil {
		return emptyItem, err
	}

	if len(response) != 1 {
		return emptyItem, fmt.Errorf("unexpected response from API")
	}

	return entities.Item{
		Base: entities.Base{
			SiteId: entities.SiteID(response[0].Body.SiteID),
			ItemId: entities.ItemID(response[0].Body.ID),
		},
		Price:      response[0].Body.Price,
		StartTime:  response[0].Body.StartTime,
		CategoryId: entities.CategoryID(response[0].Body.ID),
		CurrencyId: entities.CurrencyID(response[0].Body.CurrencyID),
		SellerId:   entities.UserID(response[0].Body.SellerID),
	}, nil
}

func (c *meliImpl) GetCategory(categoryId entities.CategoryID) (entities.Category, error) {
	emptyCategory := entities.Category{}

	if categoryId == "" {
		return emptyCategory, fmt.Errorf("category id cannot be nil")
	}

	url := getBaseUrl().JoinPath("categories").JoinPath(string(categoryId))

	res, err := doGet(c.Client, url, nil)
	if err != nil {
		return emptyCategory, err
	}

	out, err := io.ReadAll(res.Body)
	if err != nil {
		return emptyCategory, err
	}

	var response queryTypes.CategoryResponse
	if err := json.Unmarshal(out, &response); err != nil {
		return emptyCategory, err
	}

	cat := entities.Category{
		Id:   entities.CategoryID(response.ID),
		Name: response.Name,
	}

	return cat, nil
}

func (c *meliImpl) GetCurrency(currencyId entities.CurrencyID) (entities.Currency, error) {
	emptyCurrency := entities.Currency{}

	if currencyId == "" {
		return emptyCurrency, fmt.Errorf("currency id cannot be nil")
	}

	url := getBaseUrl().JoinPath("currencies").JoinPath(string(currencyId))

	res, err := doGet(c.Client, url, nil)
	if err != nil {
		return emptyCurrency, err
	}

	out, err := io.ReadAll(res.Body)
	if err != nil {
		return emptyCurrency, err
	}

	var response queryTypes.CurrencyResponse
	if err := json.Unmarshal(out, &response); err != nil {
		return emptyCurrency, err
	}

	currency := entities.Currency{
		Id:          currencyId,
		Description: response.Description,
	}

	return currency, nil
}

func (c *meliImpl) GetUser(sellerId entities.UserID) (entities.User, error) {
	emptyUser := entities.User{}

	if sellerId == 0 {
		return emptyUser, fmt.Errorf("seller id cannot be nil")
	}

	url := getBaseUrl().JoinPath("users").JoinPath(fmt.Sprint(sellerId))

	res, err := doGet(c.Client, url, nil)
	if err != nil {
		return emptyUser, err
	}

	out, err := io.ReadAll(res.Body)
	if err != nil {
		return emptyUser, err
	}

	var response queryTypes.UserResponse
	if err := json.Unmarshal(out, &response); err != nil {
		return emptyUser, err
	}

	user := entities.User{
		Id:       entities.UserID(sellerId),
		Nickname: response.Nickname,
	}

	return user, nil
}
