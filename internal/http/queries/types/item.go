package types

import "time"

type ItemResponse []struct {
	Code int  `json:"code"`
	Body Body `json:"body"`
}

type Body struct {
	ID                string      `json:"id,omitempty"`
	SiteID            string      `json:"site_id,omitempty"`
	Title             string      `json:"title"`
	Subtitle          interface{} `json:"subtitle,omitempty"`
	SellerID          int64       `json:"seller_id"`
	CategoryID        string      `json:"category_id"`
	OfficialStoreID   interface{} `json:"official_store_id,omitempty"`
	Price             float64     `json:"price"`
	BasePrice         float64     `json:"base_price,omitempty"`
	OriginalPrice     float64     `json:"original_price,omitempty"`
	CurrencyID        string      `json:"currency_id"`
	InitialQuantity   int         `json:"initial_quantity,omitempty"`
	AvailableQuantity int         `json:"available_quantity,omitempty"`
	SoldQuantity      int         `json:"sold_quantity,omitempty"`
	SaleTerms         []struct {
		ID          string      `json:"id,omitempty"`
		Name        string      `json:"name,omitempty"`
		ValueID     string      `json:"value_id,omitempty"`
		ValueName   string      `json:"value_name,omitempty"`
		ValueStruct interface{} `json:"value_struct,omitempty"`
		Values      []struct {
			ID     string      `json:"id,omitempty"`
			Name   string      `json:"name,omitempty"`
			Struct interface{} `json:"struct,omitempty"`
		} `json:"values,omitempty"`
	} `json:"sale_terms,omitempty"`
	BuyingMode      string    `json:"buying_mode,omitempty"`
	ListingTypeID   string    `json:"listing_type_id,omitempty"`
	StartTime       time.Time `json:"start_time"`
	StopTime        time.Time `json:"stop_time,omitempty"`
	Condition       string    `json:"condition,omitempty"`
	Permalink       string    `json:"permalink,omitempty"`
	ThumbnailID     string    `json:"thumbnail_id,omitempty"`
	Thumbnail       string    `json:"thumbnail,omitempty"`
	SecureThumbnail string    `json:"secure_thumbnail,omitempty"`
	Pictures        []struct {
		ID        string `json:"id,omitempty"`
		URL       string `json:"url,omitempty"`
		SecureURL string `json:"secure_url,omitempty"`
		Size      string `json:"size,omitempty"`
		MaxSize   string `json:"max_size,omitempty"`
		Quality   string `json:"quality,omitempty"`
	} `json:"pictures,omitempty"`
	VideoID                      interface{}   `json:"video_id,omitempty"`
	Descriptions                 []interface{} `json:"descriptions,omitempty"`
	AcceptsMercadopago           bool          `json:"accepts_mercadopago,omitempty"`
	NonMercadoPagoPaymentMethods []interface{} `json:"non_mercado_pago_payment_methods,omitempty"`
	Shipping                     struct {
		Mode         string        `json:"mode,omitempty"`
		Methods      []interface{} `json:"methods,omitempty"`
		Tags         []interface{} `json:"tags,omitempty"`
		Dimensions   interface{}   `json:"dimensions,omitempty"`
		LocalPickUp  bool          `json:"local_pick_up,omitempty"`
		FreeShipping bool          `json:"free_shipping,omitempty"`
		LogisticType string        `json:"logistic_type,omitempty"`
		StorePickUp  bool          `json:"store_pick_up,omitempty"`
	} `json:"shipping,omitempty"`
	InternationalDeliveryMode string `json:"international_delivery_mode,omitempty"`
	SellerAddress             struct {
		City struct {
			Name string `json:"name,omitempty"`
		} `json:"city,omitempty"`
		State struct {
			ID   string `json:"id,omitempty"`
			Name string `json:"name,omitempty"`
		} `json:"state,omitempty"`
		Country struct {
			ID   string `json:"id,omitempty"`
			Name string `json:"name,omitempty"`
		} `json:"country,omitempty"`
		SearchLocation struct {
			City struct {
				ID   string `json:"id,omitempty"`
				Name string `json:"name,omitempty"`
			} `json:"city,omitempty"`
			State struct {
				ID   string `json:"id,omitempty"`
				Name string `json:"name,omitempty"`
			} `json:"state,omitempty"`
		} `json:"search_location,omitempty"`
		ID int `json:"id,omitempty"`
	} `json:"seller_address,omitempty"`
	SellerContact interface{} `json:"seller_contact,omitempty"`
	Location      struct {
	} `json:"location,omitempty"`
	CoverageAreas []interface{} `json:"coverage_areas,omitempty"`
	Attributes    []struct {
		ID          string      `json:"id,omitempty"`
		Name        string      `json:"name,omitempty"`
		ValueID     string      `json:"value_id,omitempty"`
		ValueName   string      `json:"value_name,omitempty"`
		ValueStruct interface{} `json:"value_struct,omitempty"`
		Values      []struct {
			ID     string      `json:"id,omitempty"`
			Name   string      `json:"name,omitempty"`
			Struct interface{} `json:"struct,omitempty"`
		} `json:"values,omitempty"`
		AttributeGroupID   string `json:"attribute_group_id,omitempty"`
		AttributeGroupName string `json:"attribute_group_name,omitempty"`
	} `json:"attributes,omitempty"`
	Warnings      []interface{} `json:"warnings,omitempty"`
	ListingSource string        `json:"listing_source,omitempty"`
	Variations    []struct {
		ID                    int64   `json:"id,omitempty"`
		Price                 float64 `json:"price,omitempty"`
		AttributeCombinations []struct {
			ID          string      `json:"id,omitempty"`
			Name        string      `json:"name,omitempty"`
			ValueID     string      `json:"value_id,omitempty"`
			ValueName   string      `json:"value_name,omitempty"`
			ValueStruct interface{} `json:"value_struct,omitempty"`
			Values      []struct {
				ID     string      `json:"id,omitempty"`
				Name   string      `json:"name,omitempty"`
				Struct interface{} `json:"struct,omitempty"`
			} `json:"values,omitempty"`
		} `json:"attribute_combinations,omitempty"`
		AvailableQuantity int           `json:"available_quantity,omitempty"`
		SoldQuantity      int           `json:"sold_quantity,omitempty"`
		SaleTerms         []interface{} `json:"sale_terms,omitempty"`
		PictureIds        []string      `json:"picture_ids,omitempty"`
		CatalogProductID  interface{}   `json:"catalog_product_id,omitempty"`
	} `json:"variations,omitempty"`
	Tags []string `json:"tags,omitempty"`
}
