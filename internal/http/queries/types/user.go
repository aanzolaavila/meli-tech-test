package types

type UserResponse struct {
	ID               int    `json:"id,omitempty"`
	Nickname         string `json:"nickname"`
	RegistrationDate string `json:"registration_date,omitempty"`
	FirstName        string `json:"first_name,omitempty"`
	LastName         string `json:"last_name,omitempty"`
	CountryID        string `json:"country_id,omitempty"`
	Email            string `json:"email,omitempty"`
	Identification   struct {
		Type   string `json:"type,omitempty"`
		Number string `json:"number,omitempty"`
	} `json:"identification,omitempty"`
	Address struct {
		State   string `json:"state,omitempty"`
		City    string `json:"city,omitempty"`
		Address string `json:"address,omitempty"`
		ZipCode string `json:"zip_code,omitempty"`
	} `json:"address,omitempty"`
	Phone struct {
		AreaCode  string `json:"area_code,omitempty"`
		Number    string `json:"number,omitempty"`
		Extension string `json:"extension,omitempty"`
		Verified  bool   `json:"verified,omitempty"`
	} `json:"phone,omitempty"`
	AlternativePhone struct {
		AreaCode  string `json:"area_code,omitempty"`
		Number    string `json:"number,omitempty"`
		Extension string `json:"extension,omitempty"`
	} `json:"alternative_phone,omitempty"`
	UserType         string      `json:"user_type,omitempty"`
	Tags             []string    `json:"tags,omitempty"`
	Logo             interface{} `json:"logo,omitempty"`
	Points           int         `json:"points,omitempty"`
	SiteID           string      `json:"site_id,omitempty"`
	Permalink        string      `json:"permalink,omitempty"`
	SellerExperience string      `json:"seller_experience,omitempty"`
	SellerReputation struct {
		LevelID           interface{} `json:"level_id,omitempty"`
		PowerSellerStatus interface{} `json:"power_seller_status,omitempty"`
		Transactions      struct {
			Period    string `json:"period,omitempty"`
			Total     int    `json:"total,omitempty"`
			Completed int    `json:"completed,omitempty"`
			Canceled  int    `json:"canceled,omitempty"`
			Ratings   struct {
				Positive float64 `json:"positive,omitempty"`
				Negative float64 `json:"negative,omitempty"`
				Neutral  float64 `json:"neutral,omitempty"`
			} `json:"ratings,omitempty"`
		} `json:"transactions,omitempty"`
	} `json:"seller_reputation,omitempty"`
	BuyerReputation struct {
		CanceledTransactions int `json:"canceled_transactions,omitempty"`
		Transactions         struct {
			Period    string      `json:"period,omitempty"`
			Total     interface{} `json:"total,omitempty"`
			Completed interface{} `json:"completed,omitempty"`
			Canceled  struct {
				Total interface{} `json:"total,omitempty"`
				Paid  interface{} `json:"paid,omitempty"`
			} `json:"canceled,omitempty"`
			Unrated struct {
				Total interface{} `json:"total,omitempty"`
				Paid  interface{} `json:"paid,omitempty"`
			} `json:"unrated,omitempty"`
			NotYetRated struct {
				Total interface{} `json:"total,omitempty"`
				Paid  interface{} `json:"paid,omitempty"`
				Units interface{} `json:"units,omitempty"`
			} `json:"not_yet_rated,omitempty"`
		} `json:"transactions,omitempty"`
		Tags []interface{} `json:"tags,omitempty"`
	} `json:"buyer_reputation,omitempty"`
	Status struct {
		SiteStatus string `json:"site_status,omitempty"`
		List       struct {
			Allow            bool          `json:"allow,omitempty"`
			Codes            []interface{} `json:"codes,omitempty"`
			ImmediatePayment struct {
				Required bool          `json:"required,omitempty"`
				Reasons  []interface{} `json:"reasons,omitempty"`
			} `json:"immediate_payment,omitempty"`
		} `json:"list,omitempty"`
		Buy struct {
			Allow            bool          `json:"allow,omitempty"`
			Codes            []interface{} `json:"codes,omitempty"`
			ImmediatePayment struct {
				Required bool          `json:"required,omitempty"`
				Reasons  []interface{} `json:"reasons,omitempty"`
			} `json:"immediate_payment,omitempty"`
		} `json:"buy,omitempty"`
		Sell struct {
			Allow            bool          `json:"allow,omitempty"`
			Codes            []interface{} `json:"codes,omitempty"`
			ImmediatePayment struct {
				Required bool          `json:"required,omitempty"`
				Reasons  []interface{} `json:"reasons,omitempty"`
			} `json:"immediate_payment,omitempty"`
		} `json:"sell,omitempty"`
		Billing struct {
			Allow bool          `json:"allow,omitempty"`
			Codes []interface{} `json:"codes,omitempty"`
		} `json:"billing,omitempty"`
		MercadopagoTcAccepted  bool   `json:"mercadopago_tc_accepted,omitempty"`
		MercadopagoAccountType string `json:"mercadopago_account_type,omitempty"`
		Mercadoenvios          string `json:"mercadoenvios,omitempty"`
		ImmediatePayment       bool   `json:"immediate_payment,omitempty"`
		ConfirmedEmail         bool   `json:"confirmed_email,omitempty"`
		UserType               string `json:"user_type,omitempty"`
		RequiredAction         string `json:"required_action,omitempty"`
	} `json:"status,omitempty"`
	Credit struct {
		Consumed      int    `json:"consumed,omitempty"`
		CreditLevelID string `json:"credit_level_id,omitempty"`
	} `json:"credit,omitempty"`
}
