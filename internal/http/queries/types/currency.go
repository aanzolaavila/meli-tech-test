package types

type CurrencyResponse struct {
	ID            string `json:"id,omitempty"`
	Symbol        string `json:"symbol,omitempty"`
	Description   string `json:"description"`
	DecimalPlaces int    `json:"decimal_places,omitempty"`
}
