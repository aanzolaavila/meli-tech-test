package types

import "time"

type CategoryResponse struct {
	ID                       string `json:"id"`
	Name                     string `json:"name"`
	Picture                  string `json:"picture,omitempty"`
	Permalink                string `json:"permalink,omitempty"`
	TotalItemsInThisCategory int    `json:"total_items_in_this_category,omitempty"`
	PathFromRoot             []struct {
		ID   string `json:"id,omitempty"`
		Name string `json:"name,omitempty"`
	} `json:"path_from_root,omitempty"`
	ChildrenCategories []struct {
		ID                       string `json:"id,omitempty"`
		Name                     string `json:"name,omitempty"`
		TotalItemsInThisCategory int    `json:"total_items_in_this_category,omitempty"`
	} `json:"children_categories,omitempty"`
	AttributeTypes string `json:"attribute_types,omitempty"`
	Settings       struct {
		AdultContent            bool          `json:"adult_content,omitempty"`
		BuyingAllowed           bool          `json:"buying_allowed,omitempty"`
		BuyingModes             []string      `json:"buying_modes,omitempty"`
		CatalogDomain           string        `json:"catalog_domain,omitempty"`
		CoverageAreas           string        `json:"coverage_areas,omitempty"`
		Currencies              []string      `json:"currencies,omitempty"`
		Fragile                 bool          `json:"fragile,omitempty"`
		ImmediatePayment        string        `json:"immediate_payment,omitempty"`
		ItemConditions          []string      `json:"item_conditions,omitempty"`
		ItemsReviewsAllowed     bool          `json:"items_reviews_allowed,omitempty"`
		ListingAllowed          bool          `json:"listing_allowed,omitempty"`
		MaxDescriptionLength    int           `json:"max_description_length,omitempty"`
		MaxPicturesPerItem      int           `json:"max_pictures_per_item,omitempty"`
		MaxPicturesPerItemVar   int           `json:"max_pictures_per_item_var,omitempty"`
		MaxSubTitleLength       int           `json:"max_sub_title_length,omitempty"`
		MaxTitleLength          int           `json:"max_title_length,omitempty"`
		MaximumPrice            interface{}   `json:"maximum_price,omitempty"`
		MaximumPriceCurrency    string        `json:"maximum_price_currency,omitempty"`
		MinimumPrice            int           `json:"minimum_price,omitempty"`
		MinimumPriceCurrency    string        `json:"minimum_price_currency,omitempty"`
		MirrorCategory          interface{}   `json:"mirror_category,omitempty"`
		MirrorMasterCategory    interface{}   `json:"mirror_master_category,omitempty"`
		MirrorSlaveCategories   []interface{} `json:"mirror_slave_categories,omitempty"`
		Price                   string        `json:"price,omitempty"`
		ReservationAllowed      string        `json:"reservation_allowed,omitempty"`
		Restrictions            []interface{} `json:"restrictions,omitempty"`
		RoundedAddress          bool          `json:"rounded_address,omitempty"`
		SellerContact           string        `json:"seller_contact,omitempty"`
		ShippingProfile         string        `json:"shipping_profile,omitempty"`
		ShowContactInformation  bool          `json:"show_contact_information,omitempty"`
		SimpleShipping          string        `json:"simple_shipping,omitempty"`
		Stock                   string        `json:"stock,omitempty"`
		SubVertical             string        `json:"sub_vertical,omitempty"`
		Subscribable            bool          `json:"subscribable,omitempty"`
		Tags                    []interface{} `json:"tags,omitempty"`
		Vertical                string        `json:"vertical,omitempty"`
		VipSubdomain            string        `json:"vip_subdomain,omitempty"`
		BuyerProtectionPrograms []string      `json:"buyer_protection_programs,omitempty"`
		Status                  string        `json:"status,omitempty"`
	} `json:"settings,omitempty"`
	ChannelsSettings []interface{} `json:"channels_settings,omitempty"`
	MetaCategID      interface{}   `json:"meta_categ_id,omitempty"`
	Attributable     bool          `json:"attributable,omitempty"`
	DateCreated      time.Time     `json:"date_created,omitempty"`
}
