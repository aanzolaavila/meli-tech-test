package types

import (
	"time"
)

type Model struct {
	CreatedAt time.Time
	UpdatedAt time.Time
	// DeletedAt gorm.DeletedAt `gorm:"index"`
}

type OAuthToken struct {
	Model
	AppId        string `gorm:"primaryKey;autoIncrement:false"`
	AccessToken  string
	RefreshToken string
	TokenType    string
	Expiry       time.Time
}
