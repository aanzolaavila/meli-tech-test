package auth

import (
	"github.com/aanzolaavila/meli-technical-test/internal/auth/types"
)

type AuthRepository interface {
	AuthToken(id string) (types.Token, error)
	SaveAuthToken(id string, token types.Token) error
	DeleteToken(id string) error
}
