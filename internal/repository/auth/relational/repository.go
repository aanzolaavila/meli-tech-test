package impl

import (
	"fmt"

	"github.com/aanzolaavila/meli-technical-test/internal/auth/types"
	dbtypes "github.com/aanzolaavila/meli-technical-test/internal/repository/auth/types"
	"golang.org/x/oauth2"
	"gorm.io/gorm"
)

type AuthRepositoryRelational struct {
	DB *gorm.DB
}

func NewRepository(db *gorm.DB) (*AuthRepositoryRelational, error) {
	if db == nil {
		return nil, fmt.Errorf("db is nil")
	}

	if err := db.AutoMigrate(&dbtypes.OAuthToken{}); err != nil {
		return nil, err
	}

	return &AuthRepositoryRelational{
		DB: db,
	}, nil
}

func (r *AuthRepositoryRelational) AuthToken(id string) (types.Token, error) {
	emptyToken := types.Token{}

	if id == "" {
		return emptyToken, fmt.Errorf("token id cannot be nil")
	}

	var dbToken dbtypes.OAuthToken
	tx := r.DB.First(&dbToken, id)
	if tx.Error != nil {
		return emptyToken, fmt.Errorf("error retrieving token with id [%s]: %v", id, tx.Error)
	}

	return types.Token{
		Token: &oauth2.Token{
			AccessToken:  dbToken.AccessToken,
			TokenType:    dbToken.TokenType,
			RefreshToken: dbToken.RefreshToken,
			Expiry:       dbToken.Expiry,
		},
	}, nil
}

func (r *AuthRepositoryRelational) SaveAuthToken(id string, token types.Token) error {
	if id == "" {
		return fmt.Errorf("id cannot be nil")
	}

	dbToken := dbtypes.OAuthToken{
		AppId:        id,
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
		TokenType:    token.TokenType,
		Expiry:       token.Expiry,
	}

	tx := r.DB.Create(&dbToken)
	if tx.Error != nil {
		return fmt.Errorf("failed to create token in db: %v", tx.Error)
	}

	return nil
}

func (r *AuthRepositoryRelational) DeleteToken(id string) error {
	if id == "" {
		return fmt.Errorf("id cannot be nil")
	}

	dbToken := dbtypes.OAuthToken{
		AppId: id,
	}

	tx := r.DB.Delete(dbToken)
	if tx.Error != nil {
		return fmt.Errorf("failed to delete token in db: %v", tx.Error)
	}

	return nil
}
