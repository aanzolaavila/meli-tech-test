package repository

import "github.com/aanzolaavila/meli-technical-test/internal/entities"

type Repository interface {
	Entry(entities.ItemID) (entities.Entry, error)
	SaveEntry(entities.Entry) error
}
