package repository

import (
	"time"

	"gorm.io/gorm"
)

type Model struct {
	gorm.Model
}

type Entry struct {
	Model
	SiteId      string
	ItemId      string `gorm:"uniqueIndex"`
	Price       float64
	StartTime   time.Time
	CategoryId  string
	CurrencyId  string
	SellerId    int64
	Name        string
	Description string
	Nickname    string
}
