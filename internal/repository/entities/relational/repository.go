package impl

import (
	"fmt"

	"github.com/aanzolaavila/meli-technical-test/internal/entities"
	dbtypes "github.com/aanzolaavila/meli-technical-test/internal/repository/entities/types"
	"gorm.io/gorm"
)

type EntititesRepositoryRelational struct {
	DB *gorm.DB
}

func NewRepository(db *gorm.DB) (*EntititesRepositoryRelational, error) {
	if db == nil {
		return nil, fmt.Errorf("db cannot be nil")
	}

	if err := db.AutoMigrate(&dbtypes.Entry{}); err != nil {
		return nil, err
	}

	return &EntititesRepositoryRelational{
		DB: db,
	}, nil
}

func mapDbEntryToEntitiesEntry(entry dbtypes.Entry) entities.Entry {
	return entities.Entry{
		Item: entities.Item{
			Base: entities.Base{
				SiteId: entities.SiteID(entry.SiteId),
				ItemId: entities.ItemID(entry.ItemId),
			},
			Price:      entry.Price,
			StartTime:  entry.StartTime,
			CategoryId: entities.CategoryID(entry.CategoryId),
			CurrencyId: entities.CurrencyID(entry.CurrencyId),
			SellerId:   entities.UserID(entry.SellerId),
		},
		Category: entities.Category{
			Id:   entities.CategoryID(entry.CategoryId),
			Name: entry.Name,
		},
		Currency: entities.Currency{
			Id:          entities.CurrencyID(entry.CurrencyId),
			Description: entry.Description,
		},
		User: entities.User{
			Id:       entities.UserID(entry.SellerId),
			Nickname: entry.Nickname,
		},
	}
}

func mapEntitiesEntryToDbEntry(entry entities.Entry) dbtypes.Entry {
	return dbtypes.Entry{
		SiteId:      string(entry.Item.Base.SiteId),
		ItemId:      string(entry.Item.Base.ItemId),
		Price:       entry.Item.Price,
		StartTime:   entry.Item.StartTime,
		CategoryId:  string(entry.Item.CategoryId),
		CurrencyId:  string(entry.Item.CurrencyId),
		SellerId:    int64(entry.Item.SellerId),
		Name:        entry.Category.Name,
		Description: entry.Currency.Description,
		Nickname:    entry.User.Nickname,
	}
}

func (r *EntititesRepositoryRelational) Entry(id entities.ItemID) (entities.Entry, error) {
	emptyEntry := entities.Entry{}
	if id == "" {
		return emptyEntry, fmt.Errorf("id cannot be nil")
	}

	var entry dbtypes.Entry
	if tx := r.DB.First(&entry, id); tx.Error != nil {
		return emptyEntry, fmt.Errorf("could not get entry with id [%s]: %v", id, tx.Error)
	}

	return mapDbEntryToEntitiesEntry(entry), nil

}

func (r *EntititesRepositoryRelational) SaveEntry(entry entities.Entry) error {
	dbEntry := mapEntitiesEntryToDbEntry(entry)
	if tx := r.DB.Create(&dbEntry); tx.Error != nil {
		return fmt.Errorf("could not create entry with id [%s]: %v", entry.Item.ItemId, tx.Error)
	}

	return nil
}
