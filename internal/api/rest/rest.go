package rest

import (
	"fmt"
	"net/http"
	"os"

	"github.com/aanzolaavila/meli-technical-test/internal/entities"
	"github.com/aanzolaavila/meli-technical-test/internal/pipeline"
	"github.com/aanzolaavila/meli-technical-test/internal/pipeline/generator/file/csv"
	"github.com/aanzolaavila/meli-technical-test/internal/services"
	"github.com/gin-gonic/gin"
)

type RestServiceApi struct {
	Started      bool
	QueryService services.QueryService[entities.Base]
	Router       *gin.Engine
}

func NewRestServiceApi(service services.QueryService[entities.Base]) (*RestServiceApi, error) {
	if service == nil {
		return nil, fmt.Errorf("service cannot be nil")
	}

	r := gin.Default()

	return &RestServiceApi{
		Started:      false,
		QueryService: service,
		Router:       r,
	}, nil
}

func convertCsvEntry(entry []string) (entities.Base, error) {
	if len(entry) != 2 {
		return entities.Base{}, fmt.Errorf("entry needs to have 2 fields")
	}

	site := entry[0]
	itemId := entry[1]

	return entities.Base{
		SiteId: entities.SiteID(site),
		ItemId: entities.ItemID(itemId),
	}, nil
}

func (rs *RestServiceApi) configureService() {
	rs.Router.GET("/healthcheck", func(c *gin.Context) {
		c.String(http.StatusOK, "Service is healthy")
	})

	v1 := rs.Router.Group("/v1")
	{
		v1.POST("/queries/file/csv", rs.processCsvFileHandler())
	}
}

func (rs *RestServiceApi) processCsvFileHandler() func(*gin.Context) {
	return func(c *gin.Context) {
		fileHeader, _ := c.FormFile("file")

		file, err := fileHeader.Open()
		if err != nil {
			c.String(http.StatusBadRequest, "could not open file: %v", err)
			return
		}

		streamGen := csv.NewCsvStream(convertCsvEntry)

		stream, err := streamGen.GetGenerator(file)
		if err != nil {
			c.String(http.StatusBadRequest, "could not open file: %v", err)
			return
		}

		filteredInput := pipeline.OnError(nil, stream,
			func(base entities.Base, err error) {
				fmt.Fprintf(os.Stderr, "Error processing input with site [%s] and item id [%s]: %v\n",
					base.SiteId, base.ItemId, err)
			})

		err = rs.QueryService.ProcessAsync(filteredInput)
		if err != nil {
			c.String(http.StatusBadRequest, "error processing file: %v", err)
			return
		}

		c.String(http.StatusAccepted, "file is being processed")

	}
}

func (rs *RestServiceApi) StartService() error {
	if rs.Started {
		return fmt.Errorf("rest service is already started")
	}

	rs.configureService()

	err := rs.Router.Run()
	if err == nil {
		rs.Started = true
	}

	return err
}
