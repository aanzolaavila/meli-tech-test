package csv

import (
	"encoding/csv"
	"io"

	"github.com/aanzolaavila/meli-technical-test/internal/pipeline"
	"github.com/aanzolaavila/meli-technical-test/internal/pipeline/generator/file"
)

type converter[T any] func([]string) (T, error)

type csvStream[T any] struct {
	conv converter[T]
}

func NewCsvStream[T any](conv converter[T]) file.FileGenerator[T] {
	return &csvStream[T]{
		conv: conv,
	}
}

func (s *csvStream[T]) readRecord(r *csv.Reader) (pipeline.Result[T], error) {
	record, err := r.Read()
	if err != nil {
		return pipeline.Result[T]{}, err
	}

	v, err := s.conv(record)
	return pipeline.Result[T]{
		Error: err,
		Value: v,
	}, nil
}

func (s *csvStream[T]) GetGenerator(reader io.ReadCloser) (<-chan pipeline.Result[T], error) {
	out := make(chan pipeline.Result[T])
	r := csv.NewReader(reader)
	go func() {
		defer reader.Close()
		defer close(out)

		// ignore first row
		_, err := s.readRecord(r)
		if err == io.EOF {
			return
		}

		for {
			res, err := s.readRecord(r)
			if err == io.EOF {
				break
			}

			out <- res
		}
	}()

	return out, nil
}
