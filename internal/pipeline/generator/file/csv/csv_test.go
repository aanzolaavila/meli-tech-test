package csv

import (
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testType struct {
	Field1 string
	Field2 string
}

const testCsvFile = `
field1,field2
good1,good2
bad1
`

func Test_GetGenerator(t *testing.T) {
	expectedError := fmt.Errorf("test error")
	converter := func(in []string) (testType, error) {
		if len(in) != 2 {
			return testType{}, expectedError
		}

		return testType{
			Field1: in[0],
			Field2: in[1],
		}, nil
	}

	csvStreamer := NewCsvStream(converter)
	inputFile := io.NopCloser(strings.NewReader(testCsvFile))
	stream, err := csvStreamer.GetGenerator(inputFile)
	assert.NoError(t, err)

	result := <-stream
	assert.NoError(t, result.Error)
	assert.Equal(t, result.Value.Field1, "good1")
	assert.Equal(t, result.Value.Field2, "good2")

	result = <-stream
	assert.Error(t, expectedError)
}
