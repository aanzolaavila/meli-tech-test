package file

import (
	"io"

	"github.com/aanzolaavila/meli-technical-test/internal/pipeline"
)

type FileGenerator[T any] interface {
	GetGenerator(io.ReadCloser) (<-chan pipeline.Result[T], error)
}
