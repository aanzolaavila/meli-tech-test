package meli

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/aanzolaavila/meli-technical-test/internal/auth"
	"github.com/aanzolaavila/meli-technical-test/internal/auth/types"
	"github.com/pkg/browser"
	"golang.org/x/oauth2"
)

const (
	AuthorizationUri      = "https://auth.mercadolibre.com.co/authorization"
	TokenUri              = "https://api.mercadolibre.com/oauth/token"
	ResponseType          = "code"
	RedirectUri           = "https://127.0.0.1/oauth/redirect"
	ServerCodeWaitTimeout = 10
	ClientConnTimeout     = 5
)

type meliAuthenticatorImpl struct {
	Config *oauth2.Config
}

func GetDefaultAuthenticator() auth.Authenticator {
	return &meliAuthenticatorImpl{}
}

func initializeOAuthServer() (<-chan string, func()) {
	messages := make(chan string, 1)
	mux := http.NewServeMux()

	s := http.Server{
		Addr:         ":443",
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 90 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      mux,
	}

	cancelled := false
	cancel := func() {
		if !cancelled {
			_ = s.Shutdown(context.Background())
			defer close(messages)
			cancelled = true
		}
	}

	mux.HandleFunc("/oauth/redirect", func(w http.ResponseWriter, req *http.Request) {
		q := req.URL.Query()
		codes, ok := q["code"]
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte("Error: Did not get the auth code"))
		} else {
			w.WriteHeader(http.StatusOK)
			_, _ = w.Write([]byte("Success: Got the code"))
		}

		var code string
		if len(codes) > 0 {
			fmt.Println("Got the code")
			code = codes[0]
			messages <- code
		} else {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte("Error: Code is malformed"))
		}
	})

	go func() {
		fmt.Println("listening for code on redirect ...")
		err := s.ListenAndServeTLS("server.crt", "server.key")
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			// If the server has errors, then let's close the messages channel
			cancel()
		}
	}()

	return messages, cancel
}

func (a meliAuthenticatorImpl) GetBaseOAuthConf(appId, secretKey string) *types.Config {
	return &types.Config{
		Config: &oauth2.Config{
			ClientID:     appId,
			ClientSecret: secretKey,
			Scopes:       []string{"offline_access", "read", "write"},
			RedirectURL:  RedirectUri,
			Endpoint: oauth2.Endpoint{
				AuthURL:  AuthorizationUri,
				TokenURL: TokenUri,
			},
		},
	}
}

func (a meliAuthenticatorImpl) GetOAuthClientFromToken(ctx context.Context, conf *types.Config, tok *types.Token) *http.Client {
	client := conf.Client(ctx, tok.Token)
	client.Timeout = ClientConnTimeout * time.Second
	return client
}

func insideDocker() bool {
	if _, err := os.Stat("/.dockerenv"); err == nil {
		return true
	}
	return false
}

func waitForCode(messages <-chan string) (code string, err error) {
	if !insideDocker() {
		select {
		case code = <-messages:
		case <-time.After(ServerCodeWaitTimeout * time.Second):
			log.Println("Timeout exceeded: canceling server")
		}

		if code == "" {
			fmt.Printf("Enter the code here: ")
			if _, err = fmt.Scan(&code); err != nil {
				return "", fmt.Errorf("could not get the code from standard input: %v", err)
			}
		}
	} else {
		log.Println("I am inside docker, waiting for code on http callback ...")
		code = <-messages
	}

	return
}

func (a meliAuthenticatorImpl) GetOAuthToken(ctx context.Context, conf *types.Config) (*types.Token, error) {
	messages, cancel := initializeOAuthServer()
	defer cancel()

	url := conf.AuthCodeURL("state", oauth2.AccessTypeOffline)

	var code string
	err := browser.OpenURL(url)
	if err != nil {
		log.Printf("Web browser is not available: %v\n", err)
		fmt.Printf("Visit the URL for the auth dialog: %v\n", url)
	}

	code, err = waitForCode(messages)
	if err != nil {
		return nil, err
	}

	tok, err := conf.Exchange(ctx, code)
	if err != nil {
		return nil, err
	}

	return &types.Token{
		Token: tok,
	}, nil
}
