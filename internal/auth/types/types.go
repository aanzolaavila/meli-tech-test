package types

import "golang.org/x/oauth2"

type Config struct {
	*oauth2.Config
}

type Token struct {
	*oauth2.Token
}
