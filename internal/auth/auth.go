package auth

import (
	"context"
	"net/http"

	"github.com/aanzolaavila/meli-technical-test/internal/auth/types"
)

type Authenticator interface {
	GetBaseOAuthConf(appId, secretKey string) *types.Config
	GetOAuthToken(ctx context.Context, conf *types.Config) (*types.Token, error)
	GetOAuthClientFromToken(ctx context.Context, conf *types.Config, tok *types.Token) *http.Client
}
