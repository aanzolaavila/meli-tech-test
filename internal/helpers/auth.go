package helpers

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/aanzolaavila/meli-technical-test/internal/auth"
	"github.com/aanzolaavila/meli-technical-test/internal/auth/types"
	authrepo "github.com/aanzolaavila/meli-technical-test/internal/repository/auth"
	"golang.org/x/oauth2"
)

func GetToken(ctx context.Context, repo authrepo.AuthRepository, authenticator auth.Authenticator, config *types.Config) (*types.Token, error) {
	var authToken *types.Token
	appId := config.ClientID

	token, err := repo.AuthToken(appId)
	if err == nil {
		authToken = &types.Token{
			Token: &oauth2.Token{
				AccessToken:  token.AccessToken,
				TokenType:    token.TokenType,
				RefreshToken: token.RefreshToken,
				Expiry:       token.Expiry,
			},
		}

		if token.Expiry.After(time.Now()) {
			return authToken, nil
		} else {
			log.Println("oauth token is expired")
			_ = repo.DeleteToken(appId)
		}
	}

	authToken, err = authenticator.GetOAuthToken(ctx, config)
	if err != nil {
		return nil, err
	}

	err = repo.SaveAuthToken(appId, *authToken)
	if err != nil {
		return nil, fmt.Errorf("could not create token in DB: %v", err)
	}

	return authToken, nil
}
